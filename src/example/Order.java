package example;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Order {

	long id;
	String name;
	List<Item> itemList = new ArrayList<>(); // Это то, что заказали
	LocalDateTime createDate;

	public Order(long id, String name, List<Item> itemList, LocalDateTime createDate) {
		this.id = id;
		this.name = name;
		this.itemList = itemList;
		this.createDate = createDate;
	}

	public List<Order> orderInRange(List<Order> orderList, LocalDateTime startDate, LocalDateTime endDate) {
		return orderList.stream().filter(order -> order.getCreateDate().isAfter(startDate) && order.getCreateDate().isBefore(endDate))
				.collect(Collectors.toList());
	}

	public List<Item> itemInRange(List<Order> orderList, LocalDateTime startDate, LocalDateTime endDate) {
		return orderList.stream().filter(order -> order.getCreateDate().isAfter(startDate) && order.getCreateDate().isBefore(endDate))
				.flatMap(i -> i.getItemList().stream()
						.filter(item -> item.getName().contains("Oт".toLowerCase())))
				.collect(Collectors.toList());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
}
