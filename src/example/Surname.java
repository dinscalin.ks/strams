package example;

import java.util.List;
import java.util.stream.Collectors;

public class Surname {

	private static List<String> surname;

	public static List<String> filterSurname(List<String> surname, String firstChar) {
		return surname.stream().filter(fChar -> fChar.startsWith(firstChar)).collect(Collectors.toList());
	}
}
