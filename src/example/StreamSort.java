package example;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.BufferedReader;
import java.util.stream.Collectors;

public class StreamSort {

	public static List<String> input() throws IOException {  // получаем строку из слов и слова передаем в Лист
		String input;
		List<String> inputWords = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		input = reader.readLine();
		inputWords.addAll(Arrays.asList(input.split(" "))); // услови через пробел
		return inputWords;
	}
	public static List<String> sortedlist(List<String> inputWords) {

		return inputWords.stream().peek(e -> System.out.println("No Sorted - " + e))
				.sorted().peek(e -> System.out.println("Sorted - " + e )).collect(Collectors.toList()); // в один стрим
	}
}
