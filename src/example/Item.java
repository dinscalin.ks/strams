package example;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class Item {
	long id;
	static String name;

	public Item(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public static String getName() {
		return name;
	}
}
